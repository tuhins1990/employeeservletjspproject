package com.works.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.works.bean.User;
import com.works.dbconnection.DBConnectionUtils;

public class UserDao {
	
	
	private static String INSERT_SQL="insert into employee(name,password,email,sex,country) values(?,?,?,?,?)";
	private static String UPDATE_SQL="update employee set name=?,password=?,email=?,sex=?,country=? where id=?";
	private static String DELETE_SQL="delete from employee where id=?";
	private static String SELECT_ALL_SQL="select * from employee";
	//private static String GET_PAGINATION_SQL="select * from employee limit \" + (start - 1) + \",\" + total)";
	private static String GET_RECORD_ID_SQL="select * from employee where id=?";

	public static int save(User u) {
		int status = 0;
		try {
			Connection con = DBConnectionUtils.getConnection();
			PreparedStatement ps = con.prepareStatement(INSERT_SQL);
			ps.setString(1, u.getFirstName());
			ps.setString(2, u.getLastName());
			ps.setString(3, u.getEmail());
			ps.setString(4, u.getSex());
			ps.setString(5, u.getCountry());
			status = ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	public static int update(User u) {
		int status = 0;
		try {
			Connection con = DBConnectionUtils.getConnection();
			PreparedStatement ps = con.prepareStatement(UPDATE_SQL);
			ps.setString(1, u.getFirstName());
			ps.setString(2, u.getLastName());
			ps.setString(3, u.getEmail());
			ps.setString(4, u.getSex());
			ps.setString(5, u.getCountry());
			ps.setInt(6, u.getId());
			status = ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	public static int delete(User u) {
		int status = 0;
		try {
			Connection con = DBConnectionUtils.getConnection();
			PreparedStatement ps = con.prepareStatement(DELETE_SQL);
			ps.setInt(1, u.getId());
			status = ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e);
		}

		return status;
	}

	public static List<User> getAllRecords() {
		List<User> list = new ArrayList<User>();

		try {
			Connection con = DBConnectionUtils.getConnection();
			PreparedStatement ps = con.prepareStatement(SELECT_ALL_SQL);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				User u = new User();
				u.setId(rs.getInt("id"));
				u.setFirstName(rs.getString("name"));
				u.setLastName(rs.getString("password"));
				u.setEmail(rs.getString("email"));
				u.setSex(rs.getString("sex"));
				u.setCountry(rs.getString("country"));
				list.add(u);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return list;
	}

	public static User getRecordById(int id) {
		User u = null;
		try {
			Connection con = DBConnectionUtils.getConnection();
			PreparedStatement ps = con.prepareStatement(GET_RECORD_ID_SQL);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				u = new User();
				u.setId(rs.getInt("id"));
				u.setFirstName(rs.getString("name"));
				u.setLastName(rs.getString("password"));
				u.setEmail(rs.getString("email"));
				u.setSex(rs.getString("sex"));
				u.setCountry(rs.getString("country"));
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return u;
	}
	 public static List<User> getRecords(int start, int total) {
	        List<User> list = new ArrayList<User>();
	        try {
	            Connection con = DBConnectionUtils.getConnection();
	            //PreparedStatement ps = con.prepareStatement(GET_PAGINATION_SQL);
	            PreparedStatement ps = con.prepareStatement(
	                    "select * from Employee limit " + (start - 1) + "," + total);
	            
	            		ResultSet rs = ps.executeQuery();
	            while (rs.next()) {
	                User u = new User();
	                u.setId(rs.getInt("id"));
					u.setFirstName(rs.getString("name"));
					u.setLastName(rs.getString("password"));
					u.setEmail(rs.getString("email"));
					u.setSex(rs.getString("sex"));
					u.setCountry(rs.getString("country"));
	                list.add(u);
	            }
	            con.close();
	        } catch (Exception e) {
	            System.out.println(e.getMessage());
	        }
	        return list;
	    }
}
